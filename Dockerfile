FROM alpine:latest
WORKDIR /main

MAINTAINER masteraalish@gmail.com

RUN apk add --no-cache bash gzip postgresql-client
RUN mkdir -p /var/log/cron && touch /var/log/cron/cron.log
RUN mkdir -p /main/crons && mkdir -p /main/scripts && mkdir -p /main/backups

COPY pg_backup/src/scripts/* /main/scripts/
COPY pg_backup/src/crons/* /main/crons/

RUN mkdir -p /main/logs && touch /main/logs/logsss.log

ENTRYPOINT ["scripts/entrypoint.sh"]
CMD ["crond", "-f", "-l", "2", "-L", "/dev/stdout"]


