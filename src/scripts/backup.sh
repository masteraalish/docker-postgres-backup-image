filename=/main/backups/$(date +%d.%m.%Y-%H.%M).sql.gz

echo -e "\n$(date +%d.%m.%Y-%H:%M) Staring backup.."

export PGPASSWORD=$DB_PASS
time pg_dump -h $DB_HOST -p $DB_PORT -U $DB_USER -d $DB_NAME | gzip > $filename
export PGPASSWORD="---"

echo -e "\n$(date +%d.%m.%Y-%H:%M) Backup finished\n"

ls -la $filename

echo -e "\nDeleting old backups"
find /main/backups/ -mtime +7 -type f -delete
echo -e "Completed. Look at rest files: \n"

ls -la /main/backups/
