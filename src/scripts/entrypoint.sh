#!/bin/sh
set -e

if [[ -n "$BACKUP_CRONJOB" ]]; then
  echo "$BACKUP_CRONJOB" > /main/crons/backups.cron
  echo "" >> /main/crons/backups.cron
else
  echo "BACKUP_CRONJOB var is not set. Using default cronjob"
fi

crontab /main/crons/backups.cron

"$@"